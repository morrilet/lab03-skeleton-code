package course.labs.activitylab;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class ActivityOne extends Activity {

		// string for logcat documentation
		private final static String TAG = "Lab-ActivityOne";

		// lifecycle counts
		//TODO: Create 7 counter variables, each corresponding to a different one of the lifecycle callback methods.
		//TODO:  increment the variables' values when their corresponding lifecycle methods get called.
		private int createCounter;
		private int startCounter;
		private int resumeCounter;
		private int pauseCounter;
		private int stopCounter;
		private int restartCounter;
		private int destroyCounter;

		private TextView createView;
		private TextView startView;
		private TextView resumeView;
		private TextView pauseView;
		private TextView stopView;
		private TextView restartView;
		private TextView destroyView;

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_one, menu);
        return true;
    }

    	// lifecycle callback overrides

        @Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

			if(savedInstanceState != null) {
				LoadFromBundle(savedInstanceState);
			} else {
				LoadFromPreferences();
			}

			setContentView(R.layout.activity_one);

			createView = (TextView)findViewById(R.id.create);
			startView = (TextView)findViewById(R.id.start);
			resumeView = (TextView)findViewById(R.id.resume);
			pauseView = (TextView)findViewById(R.id.pause);
			stopView = (TextView)findViewById(R.id.stop);
			restartView = (TextView)findViewById(R.id.restart);
			destroyView = (TextView)findViewById(R.id.destroy);

			Log.i(TAG, "onCreate called");
			
			createCounter++;
			UpdateText();
		}

		@Override
		public void onStart(){
			super.onStart();

			Log.i(TAG, "onStart called");

            startCounter++;
            UpdateText();
		}

		@Override
		public void onResume(){
		    super.onResume();

		    Log.i(TAG, "onResume called");

		    resumeCounter++;
		    UpdateText();
        }

        @Override
        public void onPause(){
		    super.onPause();

		    Log.i(TAG, "onPause called");

		    pauseCounter++;
		    UpdateText();
        }

        @Override
        public void onStop(){
		    super.onStop();

		    Log.i(TAG, "onStop called");

		    stopCounter++;
		    UpdateText();

		    SaveToPreferences();
        }

        @Override
        public void onRestart(){
		    super.onRestart();

		    Log.i(TAG, "onRestart called");

		    restartCounter++;
		    UpdateText();
        }

        @Override
        public void onDestroy(){
		    super.onDestroy();

		    Log.i(TAG, "onDestroy called");

		    destroyCounter++;
		    UpdateText();
        }

		@Override
		public void onSaveInstanceState(Bundle savedInstanceState){

			savedInstanceState.putInt("create", createCounter);
			savedInstanceState.putInt("start", startCounter);
			savedInstanceState.putInt("resume", resumeCounter);
			savedInstanceState.putInt("pause", pauseCounter);
			savedInstanceState.putInt("stop", stopCounter);
			savedInstanceState.putInt("restart", restartCounter);
			savedInstanceState.putInt("destroy", destroyCounter);

			super.onSaveInstanceState(savedInstanceState);
		}

		private void SaveToPreferences() {

			SharedPreferences.Editor prefs =
					getSharedPreferences("preferences", Context.MODE_PRIVATE).edit();
			prefs.putInt("create", createCounter);
			prefs.putInt("start", startCounter);
			prefs.putInt("resume", resumeCounter);
			prefs.putInt("pause", pauseCounter);
			prefs.putInt("stop", stopCounter);
			prefs.putInt("restart", restartCounter);
			prefs.putInt("destroy", destroyCounter);
			prefs.apply();
		}

		private void LoadFromPreferences() {

        	SharedPreferences prefs = getSharedPreferences("preferences", Context.MODE_PRIVATE);

        	createCounter = prefs.getInt("create", 0);
        	startCounter = prefs.getInt("start", 0);
        	resumeCounter = prefs.getInt("resume", 0);
        	pauseCounter = prefs.getInt("pause", 0);
        	stopCounter = prefs.getInt("stop", 0);
        	restartCounter = prefs.getInt("restart", 0);
        	destroyCounter = prefs.getInt("destroy", 0);
		}

		private void LoadFromBundle(Bundle savedInstanceState) {

        	createCounter = savedInstanceState.getInt("create");
        	startCounter = savedInstanceState.getInt("start");
        	resumeCounter = savedInstanceState.getInt("resume");
        	pauseCounter = savedInstanceState.getInt("pause");
        	stopCounter = savedInstanceState.getInt("stop");
        	restartCounter = savedInstanceState.getInt("restart");
        	destroyCounter = savedInstanceState.getInt("destroy");
		}

		private void UpdateText () {

			createView.setText(getResources().getString(R.string.onCreate) + " " + createCounter);
			startView.setText(getResources().getString(R.string.onStart) + " " + startCounter);
			resumeView.setText(getResources().getString(R.string.onResume) + " " + resumeCounter);
			pauseView.setText(getResources().getString(R.string.onPause) + " " + pauseCounter);
			stopView.setText(getResources().getString(R.string.onStop) + " " + stopCounter);
			restartView.setText(getResources().getString(R.string.onRestart) + " " + restartCounter);
			destroyView.setText(getResources().getString(R.string.onDestroy) + " " + destroyCounter);
		}

		public void launchActivityTwo(View view) {
			startActivity(new Intent(this, ActivityTwo.class));
		}
}
